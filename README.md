# Corail Example
Corail version 1.0, by Benoit Varillon and David Doose
and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

Corail_example contains examples of how to use corail.

Most of the examples taken from the ROS2 documentation. 

For documentation and infromation about installation go to [corail1.gitlab.io](https://corail1.gitlab.io/)
