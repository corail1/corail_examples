// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_core/PeriodicTask.hpp"
using namespace corail_core;

#include "utils/rt_tools.hpp"
using namespace utils;

#include <iostream>

void callback()
{
    std::cout << "Hello\n";
}

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    std::cout << "begin\n";
    PeriodicTask *task = new PeriodicTask("T1", -1, 0, std::chrono::seconds(2), callback);
    std::cout << "state: " << task->get_state() << std::endl;

    std::cout << "create: " << task->create() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::cout << "configure: " << task->configure() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    auto release = std::chrono::nanoseconds(now_ns());
    release += std::chrono::seconds(2);
    std::cout << "start: " << task->start(release) << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(10));
    std::cout << "stop: " << task->stop() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    release = std::chrono::nanoseconds(now_ns());
    release += std::chrono::seconds(2);
    std::cout << "start: " << task->start(release) << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(10));
    std::cout << "stop: " << task->stop() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::cout << "cleanup: " << task->cleanup() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::cout << "terminate: " << task->terminate() << std::endl;
    std::cout << "state: " << task->get_state() << std::endl;

    std::cout << "end\n";
    delete task;
}