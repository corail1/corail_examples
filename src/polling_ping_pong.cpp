// Corail version 1.0, by David Doose

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <iostream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"
using namespace corail_core;

#include "utils/rt_tools.hpp"
using namespace utils;

using namespace std::chrono_literals;
using namespace std::placeholders;

class PollingPingPongNode : public RealTimeNode
{

    const std::string topic = "corail_example/topic";

public:
    PollingPingPongNode() : RealTimeNode("ping_pong_node"), count_(0)
    {
        publisher_ = create_publisher<std_msgs::msg::String>(topic, 1);
        periodic_ = create_rt_timer("Publisher", 0, 30, 100ms, std::bind(&PollingPingPongNode::periodic_callback_, this));

        ping_ = create_rt_timer("Ping", 0, 20, std::chrono::seconds(1), std::bind(&PollingPingPongNode::ping_callback_, this));
        pong_ = create_rt_subscription<std_msgs::msg::String>("Pong", 0, 10, 10ms, 100ms, topic, 1, std::bind(&PollingPingPongNode::pong_callback_, this, _1), false);
    };

    void periodic_callback_()
    {
        auto msg = std_msgs::msg::String();
        msg.data = "Hello, world! ";
        // RCLCPP_INFO(get_logger(),"Publishing: %s", msg.data.c_str());
        // utils::burn_cpu(50000); // C = 50ms
        publisher_->publish(msg);
    }

    void ping_callback_()
    {
        std::cout << "[Ping] Hello !\n";
        count_ = 0;
        std::cout << "[ping] pong state: " << pong_->get_state() << std::endl;
        if (pong_->get_state() == ReactiveTaskState::Configured)
        {
            pong_->start();
            std::cout << "[Ping] started Pong\n";
        }
    }

    void pong_callback_(std_msgs::msg::String::SharedPtr msg)
    {
        std::cout << "<Pong (" << ++count_ << "): " << msg->data.c_str() << ">\n";
        if (count_ >= 5)
        {
            std::cout << "<Pong> start stopping\n";
            pong_->stop();
            std::cout << "<Pong> stopping\n";
        }
    }

private:
    std::atomic<int> count_;

    corail_core::PeriodicTask::SharedPtr periodic_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;

    PeriodicTask::SharedPtr ping_;
    SubscriptionTask<std_msgs::msg::String>::SharedPtr pong_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    RealTimeExecutor exec("executor");
    exec.add_rt_node(std::make_shared<PollingPingPongNode>());
    exec.spin();
    rclcpp::shutdown();
    return 0;
}