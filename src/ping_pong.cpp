// Corail version 1.0, by David Doose

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <iostream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"

#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"
using namespace corail_core;

#include "utils/rt_tools.hpp"
using namespace utils;

class PingPongNode : public RealTimeNode
{
public:
    PingPongNode() : RealTimeNode("ping_pong_node"), count_(0)
    {
        ping_ = create_rt_timer("Ping", 0, 20, std::chrono::seconds(1), std::bind(&PingPongNode::ping_callback_, this));
        pong_ = create_rt_timer("Pong", 0, 10, std::chrono::milliseconds(100), std::bind(&PingPongNode::pong_callback_, this), false);
    };

    void ping_callback_()
    {
        std::cout << "[Ping] Hello !\n";
        count_ = 0;
        std::cout << "[ping] pong state: " << pong_->get_state() << std::endl;
        if (pong_->get_state() == PeriodicTaskState::Configured)
        {
            pong_->start();
            std::cout << "[Ping] started Pong\n";
        }
    }

    void pong_callback_()
    {
        std::cout << "<Pong " << ++count_ << ">\n";
        if (count_ >= 5)
        {
            pong_->stop();
            std::cout << "<Pong> stopping\n";
        }
    }

private:
    std::atomic<int> count_;
    PeriodicTask::SharedPtr ping_;
    PeriodicTask::SharedPtr pong_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    RealTimeExecutor exec("executor");
    exec.add_rt_node(std::make_shared<PingPongNode>());
    exec.spin();
    rclcpp::shutdown();
    return 0;
}