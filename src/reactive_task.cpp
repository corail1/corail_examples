// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <iostream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"


#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"
using namespace corail_core;

#include "utils/rt_tools.hpp"
using namespace utils;

class TestNode : public RealTimeNode
{
public:
    TestNode() : RealTimeNode("test_node")
    {
        task_1_ = create_reactive_task("T1", 1, 10, std::chrono::milliseconds(100), std::chrono::milliseconds(500),
                                        std::bind(&TestNode::loop1, this), std::bind(&TestNode::loop2, this));
    };

    bool loop1()
    {
        static bool test = true;
        std::cout << "Hello 1\n";
        test = !test;
        utils::burn_cpu(10000); // C = 10ms
        return test;
    }

    void loop2()
    {
        std::cout << "Hello 2\n";
        utils::burn_cpu(250000); // C = 250ms
    }

private:
    ReactiveTask::SharedPtr task_1_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    RealTimeExecutor exec("executor");
    exec.add_rt_node(std::make_shared<TestNode>());
    exec.spin();
    rclcpp::shutdown();
    return 0;
}