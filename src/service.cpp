// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/srv/add_two_ints.hpp"
#include "corail_core/corail_core.hpp"

#include <cstdlib>

using namespace std::chrono_literals;

void add(const std::shared_ptr<example_interfaces::srv::AddTwoInts::Request> request,
        std::shared_ptr<example_interfaces::srv::AddTwoInts::Response> response
        )
{
    response->sum = request->a + request->b;
    RCLCPP_INFO(rclcpp::get_logger("service"), "Incoming request\na: %ld" " b: %ld",
                request->a, request->b);
    RCLCPP_INFO(rclcpp::get_logger("service"), "sending back response: [%ld]", (long int)response->sum);
}

int main(int argc, char * argv[])
{
    rclcpp::init(argc,argv);
    
    auto node = std::make_shared<corail_core::RealTimeNode>("Service_Node");
    auto service = node->create_rt_service<example_interfaces::srv::AddTwoInts>("service",0,20,10ms,500ms,"corail_examples/add_two_ints",&add);

    corail_core::RealTimeExecutor exec("Executor");

    exec.add_rt_node(node);
    exec.spin();

    rclcpp::shutdown();

}