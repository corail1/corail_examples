// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <iostream>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "corail_core/corail_core.hpp"

using namespace std::chrono_literals;
using namespace std::placeholders;


class MinimalPublisher : public corail_core::RealTimeNode
{
    public:
    MinimalPublisher() : RealTimeNode("minimal_publisher"), count_(0)
    {
        publisher_ = create_publisher<std_msgs::msg::String>("corail_example/topic",1);
        timer_ = create_rt_timer("publisher",0,10,500ms,std::bind(&MinimalPublisher::timer_callback,this));
    }
    private:
        void timer_callback()
        {
            auto msg = std_msgs::msg::String();
            msg.data = "Hello, world! " + std::to_string(count_++);
            RCLCPP_INFO(get_logger(),"Publishing: %s", msg.data.c_str());
            publisher_->publish(msg);
        }
        corail_core::PeriodicTask::SharedPtr timer_;
        rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
        size_t count_;

};

class MinimalSubscriber : public rclcpp::Node
{
    public:
        MinimalSubscriber() : Node("minimal_subscriber")
        {
            sub = create_subscription<std_msgs::msg::String>("corail_example/topic", 1, std::bind(&MinimalSubscriber::topic_callback,this,_1));
        }
    private:
        void topic_callback(std_msgs::msg::String::SharedPtr msg)
        {
            RCLCPP_INFO(get_logger(), "I heard: %s", msg->data.c_str());
        }
        rclcpp::Subscription<std_msgs::msg::String>::SharedPtr sub;

    
};

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    corail_core::RealTimeExecutor exec("executor");
    auto pub = std::make_shared<MinimalPublisher>();
    auto sub = std::make_shared<MinimalSubscriber>();
    exec.add_rt_node(pub);
    exec.add_node(sub);
    exec.spin();
    rclcpp::shutdown();
}