// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <iostream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"


#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"
using namespace corail_core;

#include "utils/rt_tools.hpp"
using namespace utils;

class TestNode : public RealTimeNode
{
public:
    TestNode() : RealTimeNode("test_node")
    {
        task_1_ = create_rt_timer("T1", 1, 20, std::chrono::milliseconds(500), std::bind(&TestNode::loop1, this));
        task_2_ = create_rt_timer("T2", 1, 10, std::chrono::seconds(1), std::bind(&TestNode::loop2, this));
    };

    void loop1()
    {
        std::cout << "Hello 1\n";
        utils::burn_cpu(100000); // C = 100ms
    }

    void loop2()
    {
        std::cout << "Hello 2\n";
        utils::burn_cpu(500000); // C = 500ms
    }

private:
    PeriodicTask::SharedPtr task_1_;
    PeriodicTask::SharedPtr task_2_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    RealTimeExecutor exec("executor");
    exec.add_rt_node(std::make_shared<TestNode>());
    exec.spin();
    rclcpp::shutdown();
    return 0;
}